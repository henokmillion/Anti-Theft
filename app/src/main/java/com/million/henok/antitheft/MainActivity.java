package com.million.henok.antitheft;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    static TextView longitudeTextView;
    static TextView latitudeTextView;
    LocationManager locationManager;
    LocationListener locationListener;
    static Double latitude, longitude;

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull
            int[] grantResults) {
        switch (requestCode) {
            case 1:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getBaseContext(), "Access Granted" +
                            String.valueOf(grantResults[0]), Toast.LENGTH_LONG).show();
                    requestPermission();
                }
                break;
        }
    }

    protected void requestPermission() {
        Log.d("REQUESTING PERMISSION", "HMM....");
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 0,
                locationListener);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        longitudeTextView = (TextView) findViewById(R.id.longitudeTextView);
        latitudeTextView = (TextView) findViewById(R.id.lattitudeTextView);

        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
//                Toast.makeText(getBaseContext(), "Location changed: " + location.getLongitude()
//                        + ", "
//                        + location.getLatitude(), Toast.LENGTH_LONG).show();
                longitudeTextView.setText("Longitude: " + location.getLongitude());
                latitudeTextView.setText("Latitude: " + location.getLatitude());

                longitude = location.getLongitude();
                latitude = location.getLatitude();
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        };

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.RECEIVE_SMS) != PackageManager.PERMISSION_GRANTED
                    ) {
                requestPermissions(new String[] {
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.INTERNET,
                        Manifest.permission.RECEIVE_SMS,
                        Manifest.permission.READ_SMS

                }, 1);

                return;
            } else {
                requestPermission();
            }
        }
//        Toast.makeText(getBaseContext(), "Location changed: ", Toast.LENGTH_LONG).show();


        Location lastLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        longitudeTextView.setText("Longitude: " + lastLocation.getLongitude());
        latitudeTextView.setText("Latitude: " + lastLocation.getLatitude());



    }

}